<?php


/**
 * This class adds structure of 'tb087_presupuesto_movimiento' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:37
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb087PresupuestoMovimientoMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb087PresupuestoMovimientoMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb087PresupuestoMovimientoPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb087PresupuestoMovimientoPeer::TABLE_NAME);
		$tMap->setPhpName('Tb087PresupuestoMovimiento');
		$tMap->setClassname('Tb087PresupuestoMovimiento');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb087_presupuesto_movimiento_co_presupuesto_movimiento_seq');

		$tMap->addPrimaryKey('CO_PRESUPUESTO_MOVIMIENTO', 'CoPresupuestoMovimiento', 'BIGINT', true, null);

		$tMap->addForeignKey('CO_PARTIDA', 'CoPartida', 'BIGINT', 'tb085_presupuesto', 'ID', false, null);

		$tMap->addColumn('NU_MONTO', 'NuMonto', 'NUMERIC', false, 32);

		$tMap->addColumn('NU_ANIO', 'NuAnio', 'NUMERIC', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null);

		$tMap->addForeignKey('CO_USUARIO', 'CoUsuario', 'BIGINT', 'tb001_usuario', 'CO_USUARIO', false, null);

		$tMap->addForeignKey('CO_TIPO_MOVIMIENTO', 'CoTipoMovimiento', 'BIGINT', 'tb088_tipo_movimiento', 'ID', false, null);

		$tMap->addColumn('CO_DETALLE_COMPRA', 'CoDetalleCompra', 'BIGINT', false, null);

		$tMap->addColumn('TX_OBSERVACION', 'TxObservacion', 'VARCHAR', false, null);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', false, null);

		$tMap->addForeignKey('CO_COMPRA_SERVICIO', 'CoCompraServicio', 'BIGINT', 'tb052_compras', 'CO_COMPRAS', false, null);

		$tMap->addColumn('CO_FACTURA', 'CoFactura', 'BIGINT', false, null);

		$tMap->addColumn('MO_SALDO_ANTERIOR', 'MoSaldoAnterior', 'NUMERIC', false, 32);

		$tMap->addColumn('MO_SALDO_NUEVO', 'MoSaldoNuevo', 'NUMERIC', false, 32);

		$tMap->addColumn('CO_SOLICITUD_ANULAR', 'CoSolicitudAnular', 'BIGINT', false, null);

		$tMap->addColumn('IN_ANULAR', 'InAnular', 'BOOLEAN', false, null);

		$tMap->addColumn('IN_CERRADO', 'InCerrado', 'BOOLEAN', false, null);

	} // doBuild()

} // Tb087PresupuestoMovimientoMapBuilder
