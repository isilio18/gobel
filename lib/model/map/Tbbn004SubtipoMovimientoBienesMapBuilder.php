<?php


/**
 * This class adds structure of 'tbbn004_subtipo_movimiento_bienes' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:40
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tbbn004SubtipoMovimientoBienesMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tbbn004SubtipoMovimientoBienesMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tbbn004SubtipoMovimientoBienesPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tbbn004SubtipoMovimientoBienesPeer::TABLE_NAME);
		$tMap->setPhpName('Tbbn004SubtipoMovimientoBienes');
		$tMap->setClassname('Tbbn004SubtipoMovimientoBienes');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tbbn004_subtipo_movimiento_bienes_co_subtipo_movimiento_bienes_seq');

		$tMap->addPrimaryKey('CO_SUBTIPO_MOVIMIENTO_BIENES', 'CoSubtipoMovimientoBienes', 'BIGINT', true, null);

		$tMap->addColumn('TX_SUBTIPO_MOVIMIENTO', 'TxSubtipoMovimiento', 'VARCHAR', false, null);

		$tMap->addForeignKey('CO_USUARIO', 'CoUsuario', 'BIGINT', 'tb001_usuario', 'CO_USUARIO', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', false, null);

		$tMap->addColumn('CO_TIPO_MOVIMIENTO_BIENES', 'CoTipoMovimientoBienes', 'BIGINT', false, null);

		$tMap->addColumn('FOR_USA', 'ForUsa', 'VARCHAR', false, null);

		$tMap->addColumn('ACE_DEP', 'AceDep', 'VARCHAR', false, null);

		$tMap->addColumn('CLA_CON', 'ClaCon', 'BIGINT', false, null);

		$tMap->addColumn('COD_MO', 'CodMo', 'VARCHAR', false, null);

	} // doBuild()

} // Tbbn004SubtipoMovimientoBienesMapBuilder
