<?php


/**
 * This class adds structure of 'tb197_ac_ae_predefinida' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:39
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb197AcAePredefinidaMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb197AcAePredefinidaMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb197AcAePredefinidaPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb197AcAePredefinidaPeer::TABLE_NAME);
		$tMap->setPhpName('Tb197AcAePredefinida');
		$tMap->setClassname('Tb197AcAePredefinida');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'VARCHAR', true, null);

		$tMap->addColumn('ID_TB196_AC_PREDEFINIDA', 'IdTb196AcPredefinida', 'NUMERIC', false, null);

		$tMap->addColumn('NU_NUMERO', 'NuNumero', 'VARCHAR', false, 255);

		$tMap->addColumn('DE_NOMBRE', 'DeNombre', 'VARCHAR', false, 255);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null);

	} // doBuild()

} // Tb197AcAePredefinidaMapBuilder
