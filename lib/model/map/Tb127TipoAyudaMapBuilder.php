<?php


/**
 * This class adds structure of 'tb127_tipo_ayuda' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:38
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb127TipoAyudaMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb127TipoAyudaMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb127TipoAyudaPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb127TipoAyudaPeer::TABLE_NAME);
		$tMap->setPhpName('Tb127TipoAyuda');
		$tMap->setClassname('Tb127TipoAyuda');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb127_tipo_ayuda_co_tipo_ayuda_seq');

		$tMap->addPrimaryKey('CO_TIPO_AYUDA', 'CoTipoAyuda', 'BIGINT', true, null);

		$tMap->addColumn('TX_TIPO_AYUDA', 'TxTipoAyuda', 'VARCHAR', false, null);

	} // doBuild()

} // Tb127TipoAyudaMapBuilder
