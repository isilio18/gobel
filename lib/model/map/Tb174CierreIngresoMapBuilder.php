<?php


/**
 * This class adds structure of 'tb174_cierre_ingreso' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:39
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb174CierreIngresoMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb174CierreIngresoMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb174CierreIngresoPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb174CierreIngresoPeer::TABLE_NAME);
		$tMap->setPhpName('Tb174CierreIngreso');
		$tMap->setClassname('Tb174CierreIngreso');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb174_cierre_ingreso_co_cierre_ingreso_seq');

		$tMap->addPrimaryKey('CO_CIERRE_INGRESO', 'CoCierreIngreso', 'BIGINT', true, null);

		$tMap->addColumn('FE_DESDE', 'FeDesde', 'DATE', false, null);

		$tMap->addColumn('FE_HASTA', 'FeHasta', 'DATE', false, null);

		$tMap->addColumn('MO_COMPROMETIDO', 'MoComprometido', 'NUMERIC', false, null);

		$tMap->addColumn('MO_CAUSADO', 'MoCausado', 'NUMERIC', false, null);

		$tMap->addColumn('MO_PAGADO', 'MoPagado', 'NUMERIC', false, null);

		$tMap->addColumn('CO_USUARIO', 'CoUsuario', 'BIGINT', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'DATE', false, null);

		$tMap->addColumn('NU_ANIO', 'NuAnio', 'NUMERIC', false, null);

		$tMap->addColumn('CO_MES', 'CoMes', 'BIGINT', false, null);

	} // doBuild()

} // Tb174CierreIngresoMapBuilder
