<?php


/**
 * This class adds structure of 'tbbn003_parroquia' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:40
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tbbn003ParroquiaMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tbbn003ParroquiaMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tbbn003ParroquiaPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tbbn003ParroquiaPeer::TABLE_NAME);
		$tMap->setPhpName('Tbbn003Parroquia');
		$tMap->setClassname('Tbbn003Parroquia');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tbbn003_parroquia_co_parroquia_seq');

		$tMap->addPrimaryKey('CO_PARROQUIA', 'CoParroquia', 'BIGINT', true, null);

		$tMap->addForeignKey('CO_MUNICIPIO', 'CoMunicipio', 'BIGINT', 'tb017_municipio', 'CO_MUNICIPIO', false, null);

		$tMap->addColumn('TX_PARROQUIA', 'TxParroquia', 'VARCHAR', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null);

		$tMap->addForeignKey('CO_USUARIO', 'CoUsuario', 'BIGINT', 'tb001_usuario', 'CO_USUARIO', false, null);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', false, null);

		$tMap->addColumn('COD_TER', 'CodTer', 'VARCHAR', false, null);

	} // doBuild()

} // Tbbn003ParroquiaMapBuilder
