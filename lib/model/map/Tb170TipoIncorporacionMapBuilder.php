<?php


/**
 * This class adds structure of 'tb170_tipo_incorporacion' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:39
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb170TipoIncorporacionMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb170TipoIncorporacionMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb170TipoIncorporacionPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb170TipoIncorporacionPeer::TABLE_NAME);
		$tMap->setPhpName('Tb170TipoIncorporacion');
		$tMap->setClassname('Tb170TipoIncorporacion');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb170_tipo_incorporacion_co_tipo_incorporacion_seq');

		$tMap->addPrimaryKey('CO_TIPO_INCORPORACION', 'CoTipoIncorporacion', 'BIGINT', true, null);

		$tMap->addColumn('TX_TIPO_INCORPORACION', 'TxTipoIncorporacion', 'VARCHAR', true, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('UPDATE_AT', 'UpdateAt', 'TIMESTAMP', false, null);

		$tMap->addForeignKey('CO_USUARIO', 'CoUsuario', 'BIGINT', 'tb001_usuario', 'CO_USUARIO', false, null);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', false, null);

	} // doBuild()

} // Tb170TipoIncorporacionMapBuilder
