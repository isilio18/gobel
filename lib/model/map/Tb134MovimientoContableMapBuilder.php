<?php


/**
 * This class adds structure of 'tb134_movimiento_contable' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:38
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb134MovimientoContableMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb134MovimientoContableMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb134MovimientoContablePeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb134MovimientoContablePeer::TABLE_NAME);
		$tMap->setPhpName('Tb134MovimientoContable');
		$tMap->setClassname('Tb134MovimientoContable');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb134_movimiento_contable_co_movimiento_contable_seq');

		$tMap->addPrimaryKey('CO_MOVIMIENTO_CONTABLE', 'CoMovimientoContable', 'BIGINT', true, null);

		$tMap->addColumn('MO_ANTERIOR', 'MoAnterior', 'NUMERIC', false, null);

		$tMap->addColumn('MO_CREDITO', 'MoCredito', 'NUMERIC', false, null);

		$tMap->addColumn('MO_DEBITO', 'MoDebito', 'NUMERIC', false, null);

		$tMap->addColumn('MO_DISPONIBLE', 'MoDisponible', 'NUMERIC', false, null);

		$tMap->addForeignKey('CO_CUENTA_CONTABLE', 'CoCuentaContable', 'BIGINT', 'tb024_cuenta_contable', 'CO_CUENTA_CONTABLE', false, null);

		$tMap->addColumn('FE_MOVIMIENTO', 'FeMovimiento', 'DATE', false, null);

		$tMap->addColumn('IN_PAGADO', 'InPagado', 'BOOLEAN', false, null);

		$tMap->addColumn('FE_PAGO', 'FePago', 'DATE', false, null);

		$tMap->addForeignKey('CO_USUARIO', 'CoUsuario', 'BIGINT', 'tb001_usuario', 'CO_USUARIO', false, null);

		$tMap->addForeignKey('CO_SOLICITUD', 'CoSolicitud', 'BIGINT', 'tb026_solicitud', 'CO_SOLICITUD', false, null);

		$tMap->addColumn('CO_SOLICITUD_ANULAR', 'CoSolicitudAnular', 'BIGINT', false, null);

		$tMap->addColumn('IN_ANULAR', 'InAnular', 'BOOLEAN', false, null);

	} // doBuild()

} // Tb134MovimientoContableMapBuilder
