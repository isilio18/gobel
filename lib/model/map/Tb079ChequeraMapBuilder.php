<?php


/**
 * This class adds structure of 'tb079_chequera' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:37
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb079ChequeraMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb079ChequeraMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb079ChequeraPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb079ChequeraPeer::TABLE_NAME);
		$tMap->setPhpName('Tb079Chequera');
		$tMap->setClassname('Tb079Chequera');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb079_chequera_co_chequera_seq');

		$tMap->addPrimaryKey('CO_CHEQUERA', 'CoChequera', 'BIGINT', true, null);

		$tMap->addColumn('CO_CUENTA_BANCARIA', 'CoCuentaBancaria', 'BIGINT', false, null);

		$tMap->addColumn('TX_DESCRIPCION', 'TxDescripcion', 'VARCHAR', false, null);

		$tMap->addColumn('NRO_CHEQUERA', 'NroChequera', 'NUMERIC', false, null);

		$tMap->addColumn('ANIO_CHEQUERA', 'AnioChequera', 'NUMERIC', false, null);

		$tMap->addColumn('SERIE', 'Serie', 'VARCHAR', false, null);

		$tMap->addColumn('NRO_INICIAL', 'NroInicial', 'NUMERIC', false, null);

		$tMap->addColumn('NRO_FINAL', 'NroFinal', 'NUMERIC', false, null);

		$tMap->addColumn('CO_USUARIO', 'CoUsuario', 'BIGINT', false, null);

		$tMap->addColumn('FE_CHEQUERA', 'FeChequera', 'DATE', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('CO_ESTADO_CHEQUERA', 'CoEstadoChequera', 'BIGINT', false, null);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', false, null);

	} // doBuild()

} // Tb079ChequeraMapBuilder
