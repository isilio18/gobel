<?php


/**
 * This class adds structure of 'tb042_retencion' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:36
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb042RetencionMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb042RetencionMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb042RetencionPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb042RetencionPeer::TABLE_NAME);
		$tMap->setPhpName('Tb042Retencion');
		$tMap->setClassname('Tb042Retencion');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb042_retencion_co_retencion_seq');

		$tMap->addPrimaryKey('CO_RETENCION', 'CoRetencion', 'BIGINT', true, null);

		$tMap->addForeignKey('CO_DOCUMENTO', 'CoDocumento', 'BIGINT', 'tb007_documento', 'CO_DOCUMENTO', false, null);

		$tMap->addForeignKey('CO_TIPO_RETENCION', 'CoTipoRetencion', 'BIGINT', 'tb041_tipo_retencion', 'CO_TIPO_RETENCION', false, null);

		$tMap->addColumn('NU_VALOR', 'NuValor', 'NUMERIC', false, null);

		$tMap->addColumn('NU_SUSTRAENDO', 'NuSustraendo', 'NUMERIC', false, null);

		$tMap->addForeignKey('CO_RAMO', 'CoRamo', 'BIGINT', 'tb038_ramo', 'CO_RAMO', false, null);

		$tMap->addColumn('MO_MINIMO', 'MoMinimo', 'NUMERIC', false, null);

		$tMap->addColumn('DE_CONCEPTO', 'DeConcepto', 'VARCHAR', false, null);

		$tMap->addColumn('NU_CONCEPTO', 'NuConcepto', 'VARCHAR', false, null);

	} // doBuild()

} // Tb042RetencionMapBuilder
