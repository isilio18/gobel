<?php


/**
 * This class adds structure of 'tb008_proveedor' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:35
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb008ProveedorMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb008ProveedorMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb008ProveedorPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb008ProveedorPeer::TABLE_NAME);
		$tMap->setPhpName('Tb008Proveedor');
		$tMap->setClassname('Tb008Proveedor');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb008_proveedor_co_proveedor_seq');

		$tMap->addPrimaryKey('CO_PROVEEDOR', 'CoProveedor', 'BIGINT', true, null);

		$tMap->addColumn('TX_RAZON_SOCIAL', 'TxRazonSocial', 'VARCHAR', false, null);

		$tMap->addColumn('CO_DOCUMENTO', 'CoDocumento', 'BIGINT', false, null);

		$tMap->addColumn('TX_SIGLAS', 'TxSiglas', 'VARCHAR', false, null);

		$tMap->addColumn('TX_RIF', 'TxRif', 'VARCHAR', false, null);

		$tMap->addColumn('TX_NIT', 'TxNit', 'VARCHAR', false, null);

		$tMap->addColumn('TX_DIRECCION', 'TxDireccion', 'VARCHAR', false, null);

		$tMap->addColumn('CO_ESTADO', 'CoEstado', 'BIGINT', false, null);

		$tMap->addColumn('CO_MUNICIPIO', 'CoMunicipio', 'BIGINT', false, null);

		$tMap->addForeignKey('CO_CLASIFICACION', 'CoClasificacion', 'BIGINT', 'tb035_clasificacion_proveedor', 'CO_CLASIFICACION', false, null);

		$tMap->addColumn('TX_EMAIL', 'TxEmail', 'VARCHAR', false, null);

		$tMap->addColumn('TX_SITIO_WEB', 'TxSitioWeb', 'VARCHAR', false, null);

		$tMap->addColumn('NB_REPRESENTANTE_LEGAL', 'NbRepresentanteLegal', 'VARCHAR', false, null);

		$tMap->addColumn('NU_CEDULA_REPRESENTANTE', 'NuCedulaRepresentante', 'NUMERIC', false, null);

		$tMap->addColumn('TX_NUM_CELULAR', 'TxNumCelular', 'VARCHAR', false, null);

		$tMap->addColumn('NU_DIA_CREDITO', 'NuDiaCredito', 'NUMERIC', false, null);

		$tMap->addColumn('FE_REGISTRO', 'FeRegistro', 'DATE', false, null);

		$tMap->addForeignKey('CO_CUENTA_CONTABLE', 'CoCuentaContable', 'BIGINT', 'tb024_cuenta_contable', 'CO_CUENTA_CONTABLE', false, null);

		$tMap->addColumn('FE_VENCIMIENTO', 'FeVencimiento', 'DATE', false, null);

		$tMap->addColumn('NU_CUENTA_BANCARIA', 'NuCuentaBancaria', 'VARCHAR', false, null);

		$tMap->addForeignKey('CO_BANCO', 'CoBanco', 'BIGINT', 'tb010_banco', 'CO_BANCO', false, null);

		$tMap->addForeignKey('CO_TIPO_RESIDENCIA', 'CoTipoResidencia', 'BIGINT', 'tb036_tipo_residencia', 'CO_TIPO_RESIDENCIA', false, null);

		$tMap->addForeignKey('CO_TIPO_PROVEEDOR', 'CoTipoProveedor', 'BIGINT', 'tb037_tipo_proveedor', 'CO_TIPO_PROVEEDOR', false, null);

		$tMap->addColumn('CO_TIPO_RETENCION', 'CoTipoRetencion', 'BIGINT', false, null);

		$tMap->addColumn('TX_REGISTRO', 'TxRegistro', 'VARCHAR', false, null);

		$tMap->addColumn('FE_REGISTRO_SENIAT', 'FeRegistroSeniat', 'DATE', false, null);

		$tMap->addColumn('NU_REGISTRO', 'NuRegistro', 'NUMERIC', false, null);

		$tMap->addColumn('NU_TOMO', 'NuTomo', 'NUMERIC', false, null);

		$tMap->addColumn('NU_CAPITAL_SUSCRITO', 'NuCapitalSuscrito', 'NUMERIC', false, null);

		$tMap->addColumn('NU_CAPITAL_PAGADO', 'NuCapitalPagado', 'NUMERIC', false, null);

		$tMap->addColumn('TX_OBSERVACION', 'TxObservacion', 'VARCHAR', false, null);

		$tMap->addForeignKey('CO_IVA_RETENCION', 'CoIvaRetencion', 'BIGINT', 'tb044_iva_retencion', 'CO_IVA_RETENCION', false, null);

		$tMap->addColumn('TX_CUENTA_CONTABLE', 'TxCuentaContable', 'VARCHAR', false, null);

		$tMap->addColumn('NU_CODIGO', 'NuCodigo', 'VARCHAR', false, null);

		$tMap->addColumn('IN_RRHH', 'InRrhh', 'BOOLEAN', false, null);

		$tMap->addColumn('CO_CUENTA_ORDEN_PASIVO', 'CoCuentaOrdenPasivo', 'BIGINT', false, null);

		$tMap->addColumn('CO_CUENTA_ORDEN_ACTIVO', 'CoCuentaOrdenActivo', 'BIGINT', false, null);

	} // doBuild()

} // Tb008ProveedorMapBuilder
