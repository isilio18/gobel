<?php


/**
 * This class adds structure of 'tb067_creacion_partida' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:37
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb067CreacionPartidaMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb067CreacionPartidaMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb067CreacionPartidaPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb067CreacionPartidaPeer::TABLE_NAME);
		$tMap->setPhpName('Tb067CreacionPartida');
		$tMap->setClassname('Tb067CreacionPartida');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb067_creacion_partida_co_creacion_partida_seq');

		$tMap->addPrimaryKey('CO_CREACION_PARTIDA', 'CoCreacionPartida', 'BIGINT', true, null);

		$tMap->addForeignKey('CO_FUENTE_FINANCIAMIENTO', 'CoFuenteFinanciamiento', 'BIGINT', 'tb073_fuente_financiamiento', 'CO_FUENTE_FINANCIAMIENTO', false, null);

		$tMap->addColumn('CO_NUMERO_FUENTE', 'CoNumeroFuente', 'BIGINT', false, null);

		$tMap->addForeignKey('CO_ENTE_EJECUTOR', 'CoEnteEjecutor', 'BIGINT', 'tb082_ejecutor', 'ID', false, null);

		$tMap->addColumn('CO_PROYECTO', 'CoProyecto', 'BIGINT', false, null);

		$tMap->addForeignKey('CO_ACCION_ESPECIFICA', 'CoAccionEspecifica', 'BIGINT', 'tb084_accion_especifica', 'ID', false, null);

		$tMap->addForeignKey('CO_PARTIDA', 'CoPartida', 'BIGINT', 'tb091_partida', 'ID', false, null);

		$tMap->addColumn('NU_ANIO', 'NuAnio', 'NUMERIC', false, null);

		$tMap->addColumn('TX_DESCRIPCION', 'TxDescripcion', 'VARCHAR', false, null);

		$tMap->addColumn('TX_PARTIDA', 'TxPartida', 'VARCHAR', false, null);

		$tMap->addColumn('NU_MONTO', 'NuMonto', 'NUMERIC', false, null);

		$tMap->addColumn('CO_SOLICITUD', 'CoSolicitud', 'BIGINT', false, null);

		$tMap->addForeignKey('CO_USUARIO', 'CoUsuario', 'BIGINT', 'tb001_usuario', 'CO_USUARIO', false, null);

		$tMap->addForeignKey('CO_ESTATUS', 'CoEstatus', 'BIGINT', 'tb031_estatus_ruta', 'CO_ESTATUS_RUTA', false, null);

		$tMap->addForeignKey('CO_USUARIO_CAMBIO', 'CoUsuarioCambio', 'BIGINT', 'tb001_usuario', 'CO_USUARIO', false, null);

		$tMap->addColumn('FE_CAMBIO', 'FeCambio', 'DATE', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'DATE', false, null);

		$tMap->addForeignKey('CO_AMBITO', 'CoAmbito', 'BIGINT', 'tb138_ambito', 'CO_AMBITO', false, null);

		$tMap->addForeignKey('CO_APLICACION', 'CoAplicacion', 'BIGINT', 'tb139_aplicacion', 'CO_APLICACION', false, null);

		$tMap->addForeignKey('CO_TIPO_INGRESO', 'CoTipoIngreso', 'BIGINT', 'tb140_tipo_ingreso', 'CO_TIPO_INGRESO', false, null);

		$tMap->addColumn('NU_PARTIDA_DESAGREGADA', 'NuPartidaDesagregada', 'VARCHAR', false, null);

		$tMap->addColumn('CO_SOLICITUD_ANULAR', 'CoSolicitudAnular', 'BIGINT', false, null);

		$tMap->addColumn('IN_ANULAR', 'InAnular', 'BOOLEAN', false, null);

		$tMap->addColumn('CO_AREA_ESTRATEGICA', 'CoAreaEstrategica', 'BIGINT', false, null);

		$tMap->addColumn('CO_CLASIFICACION_ECONOMICA', 'CoClasificacionEconomica', 'BIGINT', false, null);

		$tMap->addColumn('CO_TIPO_GASTO', 'CoTipoGasto', 'VARCHAR', false, null);

	} // doBuild()

} // Tb067CreacionPartidaMapBuilder
