<?php


/**
 * This class adds structure of 'tbbn014_tipo_inmueble' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:40
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tbbn014TipoInmuebleMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tbbn014TipoInmuebleMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tbbn014TipoInmueblePeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tbbn014TipoInmueblePeer::TABLE_NAME);
		$tMap->setPhpName('Tbbn014TipoInmueble');
		$tMap->setClassname('Tbbn014TipoInmueble');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tbbn014_tipo_inmueble_co_tipo_inmueble_seq');

		$tMap->addPrimaryKey('CO_TIPO_INMUEBLE', 'CoTipoInmueble', 'BIGINT', true, null);

		$tMap->addColumn('TX_TIPO_INMUEBLE', 'TxTipoInmueble', 'VARCHAR', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null);

		$tMap->addForeignKey('CO_USUARIO', 'CoUsuario', 'BIGINT', 'tb001_usuario', 'CO_USUARIO', false, null);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', false, null);

	} // doBuild()

} // Tbbn014TipoInmuebleMapBuilder
