<?php


/**
 * This class adds structure of 'tb045_factura' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:36
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb045FacturaMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb045FacturaMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb045FacturaPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb045FacturaPeer::TABLE_NAME);
		$tMap->setPhpName('Tb045Factura');
		$tMap->setClassname('Tb045Factura');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb045_factura_co_factura_seq');

		$tMap->addPrimaryKey('CO_FACTURA', 'CoFactura', 'BIGINT', true, null);

		$tMap->addColumn('NU_FACTURA', 'NuFactura', 'VARCHAR', false, null);

		$tMap->addColumn('FE_EMISION', 'FeEmision', 'DATE', false, null);

		$tMap->addColumn('NU_BASE_IMPONIBLE', 'NuBaseImponible', 'NUMERIC', false, 32);

		$tMap->addColumn('CO_IVA_FACTURA', 'CoIvaFactura', 'NUMERIC', false, 32);

		$tMap->addColumn('NU_IVA_FACTURA', 'NuIvaFactura', 'NUMERIC', false, 32);

		$tMap->addColumn('NU_TOTAL', 'NuTotal', 'NUMERIC', false, 32);

		$tMap->addColumn('CO_IVA_RETENCION', 'CoIvaRetencion', 'NUMERIC', false, 32);

		$tMap->addColumn('NU_IVA_RETENCION', 'NuIvaRetencion', 'NUMERIC', false, 32);

		$tMap->addColumn('NU_TOTAL_RETENCION', 'NuTotalRetencion', 'NUMERIC', false, 32);

		$tMap->addColumn('TOTAL_PAGAR', 'TotalPagar', 'NUMERIC', false, 32);

		$tMap->addColumn('TX_CONCEPTO', 'TxConcepto', 'VARCHAR', false, null);

		$tMap->addForeignKey('CO_COMPRA', 'CoCompra', 'BIGINT', 'tb052_compras', 'CO_COMPRAS', false, null);

		$tMap->addForeignKey('CO_SOLICITUD', 'CoSolicitud', 'BIGINT', 'tb026_solicitud', 'CO_SOLICITUD', false, null);

		$tMap->addForeignKey('CO_PROVEEDOR', 'CoProveedor', 'BIGINT', 'tb008_proveedor', 'CO_PROVEEDOR', false, null);

		$tMap->addForeignKey('CO_RAMO', 'CoRamo', 'BIGINT', 'tb038_ramo', 'CO_RAMO', false, null);

		$tMap->addForeignKey('CO_IVA', 'CoIva', 'BIGINT', 'tb043_iva_factura', 'CO_IVA_FACTURA', false, null);

		$tMap->addForeignKey('CO_ODP', 'CoOdp', 'BIGINT', 'tb060_orden_pago', 'CO_ORDEN_PAGO', false, null);

		$tMap->addColumn('NU_CONTROL', 'NuControl', 'VARCHAR', false, null);

		$tMap->addColumn('ID_TB048_PRODUCTO', 'IdTb048Producto', 'BIGINT', false, null);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', true, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('CO_SOLICITUD_ORIGINAL', 'CoSolicitudOriginal', 'BIGINT', false, null);

		$tMap->addColumn('SUB_TOTAL_CS', 'SubTotalCs', 'NUMERIC', false, 32);

		$tMap->addColumn('NU_IVA_CS', 'NuIvaCs', 'NUMERIC', false, 32);

		$tMap->addColumn('FE_REGISTRO', 'FeRegistro', 'DATE', false, null);

		$tMap->addColumn('CO_SOLICITUD_ANULAR', 'CoSolicitudAnular', 'BIGINT', false, null);

		$tMap->addColumn('IN_ANULAR', 'InAnular', 'BOOLEAN', false, null);

		$tMap->addColumn('NU_EXENTO', 'NuExento', 'NUMERIC', false, 32);

	} // doBuild()

} // Tb045FacturaMapBuilder
