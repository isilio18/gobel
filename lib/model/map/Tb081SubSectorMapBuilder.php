<?php


/**
 * This class adds structure of 'tb081_sub_sector' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:37
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb081SubSectorMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb081SubSectorMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb081SubSectorPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb081SubSectorPeer::TABLE_NAME);
		$tMap->setPhpName('Tb081SubSector');
		$tMap->setClassname('Tb081SubSector');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb081_sub_sector_id_seq');

		$tMap->addPrimaryKey('ID', 'Id', 'BIGINT', true, null);

		$tMap->addForeignKey('ID_TB080_SECTOR', 'IdTb080Sector', 'BIGINT', 'tb080_sector', 'ID', true, null);

		$tMap->addColumn('NU_SUB_SECTOR', 'NuSubSector', 'VARCHAR', true, null);

		$tMap->addColumn('DE_SUB_SECTOR', 'DeSubSector', 'VARCHAR', true, null);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', true, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', true, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', true, null);

	} // doBuild()

} // Tb081SubSectorMapBuilder
