<?php


/**
 * This class adds structure of 'tb097_modificacion_detalle' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:37
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb097ModificacionDetalleMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb097ModificacionDetalleMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb097ModificacionDetallePeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb097ModificacionDetallePeer::TABLE_NAME);
		$tMap->setPhpName('Tb097ModificacionDetalle');
		$tMap->setClassname('Tb097ModificacionDetalle');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb097_modificacion_detalle_id_seq');

		$tMap->addPrimaryKey('ID', 'Id', 'BIGINT', true, null);

		$tMap->addForeignKey('ID_TB096_PRESUPUESTO_MODIFICACION', 'IdTb096PresupuestoModificacion', 'BIGINT', 'tb096_presupuesto_modificacion', 'ID', true, null);

		$tMap->addColumn('ID_TB013_ANIO_FISCAL', 'IdTb013AnioFiscal', 'BIGINT', true, null);

		$tMap->addColumn('ID_TB083_PROYECTO_AC', 'IdTb083ProyectoAc', 'BIGINT', false, null);

		$tMap->addForeignKey('ID_TB084_ACCION_ESPECIFICA', 'IdTb084AccionEspecifica', 'BIGINT', 'tb084_accion_especifica', 'ID', false, null);

		$tMap->addForeignKey('ID_TB085_PRESUPUESTO', 'IdTb085Presupuesto', 'BIGINT', 'tb085_presupuesto', 'ID', false, null);

		$tMap->addColumn('NU_PARTIDA', 'NuPartida', 'VARCHAR', true, null);

		$tMap->addColumn('ID_TB098_TIPO_DISTRIBUCION', 'IdTb098TipoDistribucion', 'BIGINT', true, null);

		$tMap->addColumn('MO_DISTRIBUCION', 'MoDistribucion', 'NUMERIC', true, 32);

		$tMap->addColumn('IN_ACTIVO', 'InActivo', 'BOOLEAN', false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null);

		$tMap->addColumn('ID_TB064_PRESUPUESTO_INGRESO', 'IdTb064PresupuestoIngreso', 'BIGINT', false, null);

		$tMap->addColumn('ID_TB082_EJECUTOR_ORIGEN', 'IdTb082EjecutorOrigen', 'BIGINT', false, null);

		$tMap->addColumn('ID_TB082_EJECUTOR_DESTINO', 'IdTb082EjecutorDestino', 'BIGINT', false, null);

		$tMap->addColumn('IN_TRASPASO', 'InTraspaso', 'BOOLEAN', false, null);

		$tMap->addColumn('ID_TB139_APLICACION', 'IdTb139Aplicacion', 'BIGINT', false, null);

		$tMap->addColumn('NU_APLICACION', 'NuAplicacion', 'VARCHAR', false, null);

		$tMap->addColumn('MO_DISPONIBLE', 'MoDisponible', 'NUMERIC', false, 32);

		$tMap->addColumn('CO_SOLICITUD_ANULAR', 'CoSolicitudAnular', 'BIGINT', false, null);

		$tMap->addColumn('IN_ANULAR', 'InAnular', 'BOOLEAN', false, null);

	} // doBuild()

} // Tb097ModificacionDetalleMapBuilder
