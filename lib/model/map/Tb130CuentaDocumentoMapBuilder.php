<?php


/**
 * This class adds structure of 'tb130_cuenta_documento' table to 'propel' DatabaseMap object.
 *
 *
 * This class was autogenerated by Propel 1.3.0-dev on:
 *
 * 09/09/21 21:32:38
 *
 *
 * These statically-built map classes are used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class Tb130CuentaDocumentoMapBuilder implements MapBuilder {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.Tb130CuentaDocumentoMapBuilder';

	/**
	 * The database map.
	 */
	private $dbMap;

	/**
	 * Tells us if this DatabaseMapBuilder is built so that we
	 * don't have to re-build it every time.
	 *
	 * @return     boolean true if this DatabaseMapBuilder is built, false otherwise.
	 */
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	/**
	 * Gets the databasemap this map builder built.
	 *
	 * @return     the databasemap
	 */
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	/**
	 * The doBuild() method builds the DatabaseMap
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap(Tb130CuentaDocumentoPeer::DATABASE_NAME);

		$tMap = $this->dbMap->addTable(Tb130CuentaDocumentoPeer::TABLE_NAME);
		$tMap->setPhpName('Tb130CuentaDocumento');
		$tMap->setClassname('Tb130CuentaDocumento');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('tb130_cuenta_documento_co_cuenta_documento_seq');

		$tMap->addPrimaryKey('CO_CUENTA_DOCUMENTO', 'CoCuentaDocumento', 'BIGINT', true, null);

		$tMap->addColumn('TX_DOCUMENTO', 'TxDocumento', 'VARCHAR', false, null);

		$tMap->addForeignKey('CO_CUENTA_GASTO_PAGO', 'CoCuentaGastoPago', 'BIGINT', 'tb024_cuenta_contable', 'CO_CUENTA_CONTABLE', false, null);

		$tMap->addColumn('CO_CUENTA_ORDEN_PAGO', 'CoCuentaOrdenPago', 'BIGINT', false, null);

		$tMap->addForeignKey('CO_TIPO_SOLICITUD', 'CoTipoSolicitud', 'BIGINT', 'tb027_tipo_solicitud', 'CO_TIPO_SOLICITUD', false, null);

		$tMap->addColumn('TX_SIGLA', 'TxSigla', 'VARCHAR', false, null);

		$tMap->addColumn('TX_CUENTA_GASTO', 'TxCuentaGasto', 'VARCHAR', false, null);

		$tMap->addColumn('TX_CUENTA_ODP', 'TxCuentaOdp', 'VARCHAR', false, null);

	} // doBuild()

} // Tb130CuentaDocumentoMapBuilder
