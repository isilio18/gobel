<?php

class Tb004MenuPeer extends BaseTb004MenuPeer
{
    static public function  ArmaMenu($co_rol){
  
        /*
         * Se buscan las opciones de menu padre
         */
        $c= new Criteria();
        $c->add(Tb005RolMenuPeer::CO_ROL,$co_rol);
        $c->add(Tb004MenuPeer::CO_PADRE,0);
        $c->add(Tb005RolMenuPeer::IN_VER,'t');
        $c->addJoin(Tb004MenuPeer::CO_MENU,Tb005RolMenuPeer::CO_MENU);
        $c->addAscendingOrderByColumn(Tb004MenuPeer::NU_ORDEN);
        
       
        $res = Tb004MenuPeer::doSelect($c);
        
      
        $menu = '';

        foreach($res as $resul){

                $cantidad = Tb004MenuPeer::cantidad_hijos($resul->getCoMenu(),$co_rol);
                
                if($cantidad > 0)
                {
                   
                       $menu.= "{
                                 text:'".$resul->getTxMenu()."',
                                 iconCls: '".$resul->getTxIcono()."',
                                // expanded:true,
                                 children:[".self::ArmaSubmenu($resul->getCoMenu(),$co_rol)."]
                                },";

                }
        }

        return $menu;

    }

    static public function cantidad_hijos($co_padre,$co_rol){

        $c= new Criteria();
        $c->add(Tb005RolMenuPeer::CO_ROL,$co_rol);
        $c->add(self::CO_PADRE,$co_padre);
        $c->add(Tb005RolMenuPeer::IN_VER,'t');
        $c->addJoin(self::CO_MENU,Tb005RolMenuPeer::CO_MENU);
        return self::doCount($c);
    }

    static public function ArmaSubmenu($co_padre,$co_rol){

        
        
        $c= new Criteria();
        $c->add(Tb005RolMenuPeer::CO_ROL,$co_rol);
        $c->add(self::CO_PADRE,$co_padre);
        $c->add(Tb005RolMenuPeer::IN_VER,'t');
        $c->addJoin(self::CO_MENU,Tb005RolMenuPeer::CO_MENU);
        $c->addAscendingOrderByColumn(self::NU_ORDEN);
        $res = self::doSelect($c);

        $submenu = '';

        foreach($res as $result){



            $cantidad = self::cantidad_hijosPrivilegio($result->getCoMenu(),$co_rol);

           
            
            if($cantidad > 0)
            {
                 $cantidad_hijos = self::cantidad_hijos($result->getCoMenu(),$co_rol);

                 if($cantidad_hijos > 0){
                       $submenu.= "{
                                 text:'".$result->getTxMenu()."',
                                 children:[".self::ArmaSubmenu($result->getCoMenu(),$co_rol,$co_usuario)."]
                                },";
                 }

            }else{

                $submenu.= "{
                                    text:'".$result->getTxMenu()."',
                                    iconCls:'".$result->getTxIcono()."',
                                    id:'".$result->getCoMenu()."',
                                    leaf:true,
                                    listeners :{

                                        click: function(){
                                            var msg = Ext.get('tabPrincipal');
                                                msg.load({
                                                        url: '".$result->getTxHref()."',
                                                        scripts: true,
                                                        text: 'Cargando...'
                                                });
						panel_detalle.collapse();
                                        }
                                    }
                                },";
            }


        }

        return  $submenu;



    }
    
   

    static public function getListaMenu($co_rol){

          $c = new Criteria();
          $c->clearSelectColumns();
          $c->addSelectColumn(self::CO_MENU);
          $c->addSelectColumn(self::TX_MENU);
          $c->addSelectColumn(self::CO_PADRE);
          $c->addSelectColumn(Tb005RolMenuPeer::IN_VER);
          $c->addJoin(self::CO_MENU, Tb005RolMenuPeer::CO_MENU);
          $c->addAscendingOrderByColumn(self::NU_ORDEN);
          $c->add(self::CO_PADRE,0);
          $c->add(Tb005RolMenuPeer::CO_ROL,$co_rol);
          $stmt = self::doSelectStmt($c);

          $data="";
          while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

                $data.= "new Ext.form.FieldSet({
                        defaultType: 'checkbox',
                        title: '".$row['tx_menu']."',
                        items:[".self::getListaSubmenu($row['co_menu'], $co_rol)."]}),";
          }
          return $data;

    }


    static public function getListaSubmenu($co_padre,$co_rol){

           $c = new Criteria();
          $c->clearSelectColumns();
          $c->addSelectColumn(self::CO_MENU);
          $c->addSelectColumn(self::TX_MENU);
          $c->addSelectColumn(self::CO_PADRE);
          $c->addSelectColumn(TTb005RolMenuPeer::IN_VER);
          $c->addSelectColumn(TTb005RolMenuPeer::CO_ROL_MENU);
          $c->addJoin(self::CO_MENU, TTb005RolMenuPeer::CO_MENU);
          $c->addAscendingOrderByColumn(self::NU_ORDEN);
          $c->add(self::CO_PADRE,$co_padre);
          $c->add(TTb005RolMenuPeer::CO_ROL,$co_rol);
          $stmt = self::doSelectStmt($c);

        $submenu = '';

        $stmt = self::doSelectStmt($c);

        $data="";
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){

//                if($row['in_ver']=='t')
//                    $flag = true;
//                else
                    $flag = false;

                $submenu.= "{
                                fieldLabel: '',
                                boxLabel: '".$row['tx_menu']."',";

                $submenu.=  ($row['in_ver']=='t')?'checked:true,':'';

                 $submenu.="name: 'radio[".$row['co_rol_menu']."]'},";


        }

        return  $submenu;

    }

    static public function getVerificaMenu($co_rol){

          $c = new Criteria();

          $c = new Criteria();
          $c->clearSelectColumns();
          $c->addSelectColumn(self::CO_MENU);
          $c->addSelectColumn(self::TX_MENU);
          $c->addSelectColumn(TTb005RolMenuPeer::CO_ROL_MENU);
          $c->addJoin(self::CO_MENU, TTb005RolMenuPeer::CO_MENU);
          $c->addAscendingOrderByColumn(self::NU_ORDEN);
          $c->add(TTb005RolMenuPeer::CO_ROL,$co_rol);
          $c->add(self::CO_PADRE,0,Criteria::NOT_EQUAL);

          return self::doSelectStmt($c);



    }

     static public function cantidad_hijosPrivilegio($co_padre,$co_rol){

        $c= new Criteria();
        $c->add(Tb005RolMenuPeer::CO_ROL,$co_rol);
        $c->add(self::CO_PADRE,$co_padre);
//        $c->add(T04RolMenuPeer::IN_VER,'t');
        $c->addJoin(self::CO_MENU,Tb005RolMenuPeer::CO_MENU);
        return self::doCount($c);
    }

    static public function  ArmaMenuPrivilegio($co_rol){


        /*
         * Se buscan las opciones de menu padre
         */
        $c= new Criteria();
        $c->addSelectColumn(Tb005RolMenuPeer::CO_ROL_MENU);
        $c->addSelectColumn(self::CO_MENU);
        $c->addSelectColumn(self::TX_MENU);
        $c->addSelectColumn(self::TX_ICONO);

        $c->addSelectColumn(Tb005RolMenuPeer::IN_VER);
        $c->add(Tb005RolMenuPeer::CO_ROL,$co_rol);
        $c->add(self::CO_PADRE,0);
//        $c->add(Tb005RolMenuPeer::IN_VER,'t');
        $c->addJoin(self::CO_MENU,Tb005RolMenuPeer::CO_MENU);
        $c->addAscendingOrderByColumn(self::NU_ORDEN);
        $res = self::doSelectStmt($c);

        $menu = '';
        foreach($res as $resul){

                $cantidad = self::cantidad_hijosPrivilegio($resul['co_menu'],$co_rol);

                if($cantidad > 0)
                {

                       $menu.= "{
                                 text:'".$resul['tx_menu']."',
				expanded: true,
                                 children:[".self::ArmaSubmenuPrivilegio($resul['co_menu'],$co_rol)."]
                                },";

                       

                }
        }

        return $menu;

    }

    static public function ArmaSubmenuPrivilegio($co_padre,$co_rol){

        $c= new Criteria();

        $c->addSelectColumn(Tb005RolMenuPeer::CO_ROL_MENU);
        $c->addSelectColumn(self::CO_MENU);
        $c->addSelectColumn(self::TX_MENU);
        $c->addSelectColumn(self::TX_ICONO);
        $c->addSelectColumn(Tb005RolMenuPeer::IN_VER);
        
        $c->add(Tb005RolMenuPeer::CO_ROL,$co_rol);
        $c->add(self::CO_PADRE,$co_padre);
//        $c->add(Tb005RolMenuPeer::IN_VER,'t');
        $c->addJoin(self::CO_MENU,Tb005RolMenuPeer::CO_MENU);
        $c->addAscendingOrderByColumn(self::NU_ORDEN);
        $res = self::doSelectStmt($c);


        $submenu = '';

        foreach($res as $result){

            $cantidad = self::cantidad_hijosPrivilegio($result['co_menu'],$co_rol);

            if($cantidad > 0)
            {

                       $submenu.= "{
                                 text:'".$result['tx_menu']."',
                                 id:'".$result['co_rol_menu']."',
                                 children:[".self::ArmaSubmenuPrivilegio($result['co_menu'],$co_rol)."]
                                 },";

                      

            }else{

                $submenu.= "{
                                    text:'".$result['tx_menu']."',
                                    id:'".$result['co_rol_menu']."',
                                    iconCls:'".$result['tx_icono']."',
                                    leaf:true, ";
                if($result['in_ver']==1)
                 $submenu.= "       checked: true },";
                else
                 $submenu.= "       checked: false },";
            }

        }

        return  $submenu;

    }
}
