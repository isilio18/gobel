<script type="text/javascript">
Ext.ns("ConfiguracionRutaEditar");
ConfiguracionRutaEditar.main = {
init:function(){

this.OBJ = paqueteComunJS.funcion.doJSON({stringData:'<?php echo $data ?>'});
//<Stores de fk>
this.storeCO_TIPO_SOLICITUD = this.getStoreCO_TIPO_SOLICITUD();
//<Stores de fk>
//<Stores de fk>
this.storeCO_PROCESO = this.getStoreCO_PROCESO();
//<Stores de fk>

//<ClavePrimaria>
this.co_configuracion = new Ext.form.Hidden({
    name:'co_configuracion',
    value:this.OBJ.co_configuracion});

this.co_tipo_solicitud = new Ext.form.Hidden({
    name:'tb032_configuracion_ruta[co_tipo_solicitud]',
    value:this.OBJ.co_tipo_solicitud});

this.co_proceso = new Ext.form.ComboBox({
	fieldLabel:'Proceso',
	store: this.storeCO_PROCESO,
	typeAhead: true,
	valueField: 'co_proceso',
	displayField:'tx_proceso',
	hiddenName:'tb032_configuracion_ruta[co_proceso]',
	//readOnly:(this.OBJ.co_proceso!='')?true:false,
	//style:(this.main.OBJ.co_proceso!='')?'background:#c9c9c9;':'',
	forceSelection:true,
	resizable:true,
	triggerAction: 'all',
	emptyText:'Seleccione...',
	selectOnFocus: true,
	mode: 'local',
	width:250,
	resizable:true,
	allowBlank:false
});
this.storeCO_PROCESO.load();
	paqueteComunJS.funcion.seleccionarComboByCo({
	objCMB: this.co_proceso,
	value:  this.OBJ.co_proceso,
	objStore: this.storeCO_PROCESO
});

this.nu_orden = new Ext.form.NumberField({
	fieldLabel:'Orden',
	name:'tb032_configuracion_ruta[nu_orden]',
	value:this.OBJ.nu_orden,
        width:50,
	allowBlank:false
});

this.in_cargar_dato = new Ext.form.Checkbox({
	fieldLabel:'Cargar Datos',
	name:'tb032_configuracion_ruta[in_cargar_dato]',
	checked:(this.OBJ.in_cargar_dato=='1') ? true:false
});

this.in_incompleto = new Ext.form.Checkbox({
	fieldLabel:'Tramite Incompleto',
	name:'tb032_configuracion_ruta[in_incompleto]',
	checked:(this.OBJ.in_incompleto=='1') ? true:false
});

this.nb_reporte_orden = new Ext.form.TextField({
	fieldLabel:'Nombre del Reporte',
	name:'tb032_configuracion_ruta[nb_reporte_orden]',
	value:this.OBJ.nb_reporte_orden,
        width:200
//	allowBlank:false
});

this.tx_modulo = new Ext.form.TextField({
	fieldLabel:'Modulo',
	name:'tb032_configuracion_ruta[tx_modulo]',
	value:this.OBJ.tx_modulo,
        width:200
//	allowBlank:false
});

this.tx_url = new Ext.form.TextField({
	fieldLabel:'Acción',
	name:'tb032_configuracion_ruta[tx_url]',
	value:this.OBJ.tx_url,
        width:200
//	allowBlank:false
});

this.guardar = new Ext.Button({
    text:'Guardar',
    iconCls: 'icon-guardar',
    handler:function(){

        if(!ConfiguracionRutaEditar.main.formPanel_.getForm().isValid()){
            Ext.Msg.alert("Alerta","Debe ingresar los campos en rojo");
            return false;
        }
        ConfiguracionRutaEditar.main.formPanel_.getForm().submit({
            method:'POST',
            url:'<?php echo $_SERVER["SCRIPT_NAME"] ?>/ConfiguracionRuta/guardar',
            waitMsg: 'Enviando datos, por favor espere..',
            waitTitle:'Enviando',
            failure: function(form, action) {
                Ext.MessageBox.alert('Error en transacción', action.result.msg);
            },
            success: function(form, action) {
                 if(action.result.success){
                     Ext.MessageBox.show({
                         title: 'Mensaje',
                         msg: action.result.msg,
                         closable: false,
                         icon: Ext.MessageBox.INFO,
                         resizable: false,
			 animEl: document.body,
                         buttons: Ext.MessageBox.OK
                     });
                 }
                 ConfiguracionRutaLista.main.store_lista.load();
                 ConfiguracionRutaEditar.main.winformPanel_.close();
             }
        });

   
    }
});

this.salir = new Ext.Button({
    text:'Salir',
//    iconCls: 'icon-cancelar',
    handler:function(){
        ConfiguracionRutaEditar.main.winformPanel_.close();
    }
});

this.formPanel_ = new Ext.form.FormPanel({
    frame:true,
    width:400,
    autoHeight:true,  
    autoScroll:true,
    bodyStyle:'padding:10px;',
    items:[
                    this.co_configuracion,
                    this.co_tipo_solicitud,
                    this.co_proceso,
                    this.nu_orden,
                    this.nb_reporte_orden,
                    this.in_cargar_dato,
                    this.in_incompleto,
                    this.tx_modulo,
                    this.tx_url 
            ]
});

this.winformPanel_ = new Ext.Window({
    title:'Configuración Ruta',
    modal:true,
    constrain:true,
    width:400,
    frame:true,
    closabled:true,
    autoHeight:true,
    items:[
        this.formPanel_
    ],
    buttons:[
        this.guardar,
        this.salir
    ],
    buttonAlign:'center'
});
this.winformPanel_.show();
ConfiguracionRutaLista.main.mascara.hide();
}
,getStoreCO_TIPO_SOLICITUD:function(){
    this.store = new Ext.data.JsonStore({
        url:'<?php echo $_SERVER["SCRIPT_NAME"] ?>/ConfiguracionRuta/storefkcotiposolicitud',
        root:'data',
        fields:[
            {name: 'co_tipo_solicitud'}
            ]
    });
    return this.store;
}
,getStoreCO_PROCESO:function(){
    this.store = new Ext.data.JsonStore({
        url:'<?php echo $_SERVER["SCRIPT_NAME"] ?>/ConfiguracionRuta/storefkcoproceso',
        root:'data',
        fields:[
            {name: 'co_proceso'},
            {name: 'tx_proceso'}
            ]
    });
    return this.store;
}
};
Ext.onReady(ConfiguracionRutaEditar.main.init, ConfiguracionRutaEditar.main);
</script>
