<script type="text/javascript">
Ext.ns("ContabilidadEditar");
ContabilidadEditar.main = {
init:function(){

this.OBJ = paqueteComunJS.funcion.doJSON({stringData:'<?php echo $data ?>'});
this.storeCO_TIPO_ODP = this.getStoreCO_TIPO_ODP();
 
this.detalle_factura = '';
this.monto_total = 0;
this.monto_total_factura = 0;

this.co_compra = new Ext.form.Hidden({
    name:'co_compra',
    value:this.OBJ.co_compras
});

this.co_proveedor = new Ext.form.Hidden({
    name:'co_proveedor',
    value:this.OBJ.co_proveedor
});

this.co_solicitud = new Ext.form.Hidden({
    name:'co_solicitud',
    value:this.OBJ.co_solicitud
});


this.co_documento = new Ext.form.Hidden({
    name:'co_documento',
    value:this.OBJ.co_documento
});

this.co_ramo = new Ext.form.Hidden({
    name:'co_ramo',
    value:this.OBJ.co_ramo
});
//</ClavePrimaria>

this.store_lista = this.getLista();


this.Registro = Ext.data.Record.create([
         {name: 'nu_factura', type: 'number'},
         {name: 'nu_control', type: 'number'},
         {name: 'fe_emision', type: 'string'},
         {name: 'nu_base_imponible', type:'number'},
         {name: 'co_iva_factura', type:'number'},
         {name: 'nu_iva_factura', type: 'number'},                 
         {name: 'nu_total', type:'number'},
         {name: 'co_iva_retencion', type: 'number'},
         {name: 'nu_iva_retencion', type: 'number'},
         {name: 'nu_total_retencion', type:'number'},
         {name: 'total_pagar', type:'number'},   
         {name: 'nu_total_pagar', type:'number'},
         {name: 'tx_concepto', type:'number'},
         {name: 'json_detalle_retencion', type:'string'}
]);

this.hiddenJsonFactura  = new Ext.form.Hidden({
        name:'json_factura',
        value:''
});


this.agregar = new Ext.Button({
    text: 'Agregar',
    iconCls: 'icon-nuevo',
    handler: function () {
        this.msg = Ext.get('formularioAgregar');
        this.msg.load({
            url: '<?php echo $_SERVER["SCRIPT_NAME"] ?>/Contabilidad/agregarFactura',
            scripts: true,
            text: "Cargando..",
            params:{
                co_documento: ContabilidadEditar.main.OBJ.co_documento,
                nu_iva_retencion: ContabilidadEditar.main.OBJ.nu_iva_retencion,
                co_ramo: ContabilidadEditar.main.OBJ.co_ramo,
                nu_iva:ContabilidadEditar.main.OBJ.nu_iva,
                co_solicitud: ContabilidadEditar.main.OBJ.co_solicitud
            }
        });
    }
});

this.ver_detalle = new Ext.Button({
    text: 'Ver Detalle',
    iconCls: 'icon-buscar',
    handler: function () {
        this.msg = Ext.get('formularioAgregar');
        this.msg.load({
            url: '<?php echo $_SERVER["SCRIPT_NAME"] ?>/Contabilidad/verDetalle',
            scripts: true,
            text: "Cargando.."
        });
    }
});

this.botonEliminar = new Ext.Button({
                text:'Eliminar',
                iconCls: 'icon-eliminar',
                handler: function(){
                    ContabilidadEditar.main.eliminar();
                }
});

this.botonEliminar.disable();
this.ver_detalle.disable();

this.monto_total = new Ext.form.DisplayField({
 value:"<span style='font-size:12px;'><b>|  Total a Pagar: </b>0,00</b></span>"
});

this.monto_total_compra = new Ext.form.DisplayField({
 value:"<span style='font-size:12px;'><b>Monto Total: </b>0,00</b></span>"
});

function renderMonto(val, attr, record) { 
     return paqueteComunJS.funcion.getNumeroFormateado(val);     
} 

this.nu_orden_pago = new Ext.form.TextField({
	fieldLabel:'Número Orden',
	name:'nu_orden_pago',
	value:this.OBJ.nu_orden_pago,
	width:200
});

this.gridPanel = new Ext.grid.GridPanel({
        title:'Detalle',
        iconCls: 'icon-libro',
        store: this.store_lista,
        loadMask:true,
        height:300,  
        width:810,
        columns: [
        new Ext.grid.RowNumberer(),
            {header: 'co_detalle_compras', hidden: true,width:80, menuDisabled:true,dataIndex: 'co_detalle_compras'},    
            {header: 'co_partida', hidden: true,width:80, menuDisabled:true,dataIndex: 'co_partida'}, 
            {header: 'co_tipo_movimiento', hidden: true,width:80, menuDisabled:true,dataIndex: 'co_tipo_movimiento'}, 
//            {header: 'Estatus',width:130, menuDisabled:true,dataIndex: 'tx_tipo_movimiento'},    
            {header: 'Material',width:620, menuDisabled:true,dataIndex: 'tx_producto',renderer:textoLargo},                
//            {header: 'Cod. Partida', width:100, menuDisabled:true,dataIndex: 'nu_partida'},
//            {header: 'Partida',width:220, menuDisabled:true,dataIndex: 'de_partida',renderer:textoLargo},
//            {header: 'Monto Disponible',width:180, menuDisabled:true,dataIndex: 'mo_disponible',renderer:renderMontoDisponible},
            {header: 'Monto',width:180, menuDisabled:true,dataIndex: 'monto',renderer:renderMonto}
        ], 
        bbar: new Ext.ux.StatusBar({
            id: 'basic-statusbar',
            autoScroll:true,
            defaults:{style:'color:white;font-size:30px;',autoWidth:true},
            items:[
             this.monto_total
            ]
        }),
        stripeRows: true,
        autoScroll:true,
        stateful: true       
});

if(this.OBJ.co_compras!=''){

    ContabilidadEditar.main.store_lista.baseParams.co_compra=this.OBJ.co_compras;
    this.store_lista.load({
        callback: function(){
           ContabilidadEditar.main.calcularMonto();
        }
    });

}

this.datos  = '<p class="registro_detalle"><b>RIF: </b>'+this.OBJ.tipo+'-'+this.OBJ.tx_rif+'</p>';
this.datos += '<p class="registro_detalle"><b>Razón Social: </b>'+this.OBJ.tx_razon_social+'</p>';
this.datos +='<p class="registro_detalle"><b>Dirección: </b>'+this.OBJ.tx_direccion+'</p>';

this.datos2  = '<p class="registro_detalle"><b>Tipo de Servicio: </b>'+this.OBJ.cod_producto+' - '+this.OBJ.tx_producto+'</p>';
this.datos2 += '<p class="registro_detalle"><b>Descripción: </b>'+this.OBJ.tx_observacion+'</p>';

this.fieldDatos= new Ext.form.FieldSet({
        title: 'Datos del Proveedor',
        html: this.datos
});

this.fieldDatosServicio = new Ext.form.FieldSet({
        title: 'Datos del Servicio Basico',
        html: this.datos2
});   

this.co_tipo_odp = new Ext.form.ComboBox({
	fieldLabel:'Tipo',
	store: this.storeCO_TIPO_ODP,
	typeAhead: true,
	valueField: 'co_tipo_odp',
        id:'co_tipo_odp',
	displayField:'tx_tipo_odp',
	hiddenName:'co_tipo_odp',
	forceSelection:true,
	resizable:true,
	triggerAction: 'all',
	selectOnFocus: true,
	mode: 'local',
	width:100,
	allowBlank:false
});


this.storeCO_TIPO_ODP.load({
    callback: function(){
        ContabilidadEditar.main.co_tipo_odp.setValue(ContabilidadEditar.main.OBJ.co_tipo_odp);
    }
});

this.field_tipo_odp = new Ext.form.FieldSet({
        title: 'Tipo de Orden de Pago',
        items:[this.co_tipo_odp]
});  

this.guardar = new Ext.Button({
    text:'Generar ODP',
    iconCls: 'icon-guardar',
    handler:function(){

        if(!ContabilidadEditar.main.formPanel_.getForm().isValid()){
            Ext.Msg.alert("Alerta","Debe ingresar los campos en rojo");
            return false;
        }
      
        ContabilidadEditar.main.setGuardar();
    }
});

if(this.OBJ.co_orden_pago!=''){
    this.guardar.disable();
}

this.salir = new Ext.Button({
    text:'Salir',
//    iconCls: 'icon-cancelar',
    handler:function(){
        ContabilidadEditar.main.winformPanel_.close();
    }
});

this.calcularMonto();

this.formPanel_ = new Ext.form.FormPanel({
    frame:true,
    width:850,
    autoHeight:true,  
    autoScroll:true,
    bodyStyle:'padding:10px;',
    items:[         this.co_compra,
                    this.co_proveedor,
                    this.co_documento,
                    this.co_ramo,
                    this.co_solicitud,
                    this.hiddenJsonFactura,
                    this.fieldDatos,
                    this.field_tipo_odp,
                  //  this.fieldDatosServicio,
                    this.gridPanel
            ]
});

this.winformPanel_ = new Ext.Window({
    title:'Presupuesto',
    modal:true,
    constrain:true,
    width:850,
    frame:true,
    closabled:true,
    autoHeight:true,
    items:[
        this.formPanel_
    ],
    buttons:[
        this.guardar,
        this.salir
    ],
    buttonAlign:'center'
});
this.winformPanel_.show();
},getStoreCO_TIPO_ODP: function(){
     this.store = new Ext.data.JsonStore({
        url:'<?php echo $_SERVER["SCRIPT_NAME"] ?>/Presupuesto/storefkcotipoodp',
        root:'data',
        fields:[
            {name: 'co_tipo_odp'},
            {name: 'tx_tipo_odp'}
        ]
    });
    return this.store;
},
setGuardar: function(){
    
        ContabilidadEditar.main.monto_total_factura = paqueteComunJS.funcion.getSumaColumnaGrid({
                 store:ContabilidadEditar.main.store_lista,
                 campo:'nu_total'
        });
    
        var list_factura = paqueteComunJS.funcion.getJsonByObjStore({
                store:ContabilidadEditar.main.gridPanel.getStore()
        });
        
        ContabilidadEditar.main.hiddenJsonFactura.setValue(list_factura);        
        
        ContabilidadEditar.main.formPanel_.getForm().submit({
            method:'POST',
            url:'<?php echo $_SERVER["SCRIPT_NAME"] ?>/Presupuesto/generarODPServicio',
            waitMsg: 'Enviando datos, por favor espere..',
            waitTitle:'Enviando',
            failure: function(form, action) {
                Ext.MessageBox.alert('Error en transacción', action.result.msg);
            },
            success: function(form, action) {
                 if(action.result.success){
                     Ext.MessageBox.show({
                         title: 'Mensaje',
                         msg: action.result.msg,
                         closable: false,
                         icon: Ext.MessageBox.INFO,
                         resizable: false,
			 animEl: document.body,
                         buttons: Ext.MessageBox.OK
                     });
                 }
                 
              
                Detalle.main.store_lista.load();
                
                ContabilidadEditar.main.winformPanel_.close();
             }
        });
},
calcularMonto: function(){
            ContabilidadEditar.main.total_pagar = paqueteComunJS.funcion.getSumaColumnaGrid({
                 store:ContabilidadEditar.main.store_lista,
                 campo:'monto'
            });
            
            ContabilidadEditar.main.monto_total.setValue("<span style='font-size:12px;'><b>|  Total a Pagar: </b>"+paqueteComunJS.funcion.getNumeroFormateado(ContabilidadEditar.main.total_pagar)+"</b></span>");     

},
eliminar:function(){
        var s = ContabilidadEditar.main.gridPanel.getSelectionModel().getSelections();
        
        var co_factura = ContabilidadEditar.main.gridPanel.getSelectionModel().getSelected().get('co_factura');
       
        if(co_factura!=''){
            
            Ext.Ajax.request({
            method:'POST',
            url:'<?php echo $_SERVER["SCRIPT_NAME"] ?>/Contabilidad/eliminarFactura',
            params:{
                co_factura: co_factura,
                co_solicitud: ContabilidadEditar.main.OBJ.co_solicitud
            },
            success:function(result, request ) {
               ContabilidadEditar.main.store_lista.load();
               ContabilidadEditar.main.calcularMonto();
            }});
            
        }
        
        
       
        for(var i = 0, r; r = s[i]; i++){
              ContabilidadEditar.main.store_lista.remove(r);
        }
        
},getLista: function(){
    this.store = new Ext.data.JsonStore({
    url:'<?php echo $_SERVER["SCRIPT_NAME"] ?>/Presupuesto/storelista',
    root:'data',
    fields:[
                {name: 'co_partida'},
                {name :'co_detalle_compras'},
                {name :'tx_producto'},
                {name :'nu_partida'},
                {name :'de_partida'},
                {name :'mo_disponible'},
                {name :'monto'},
                {name :'co_tipo_movimiento'},
                {name: 'tx_tipo_movimiento'}
           ]
    });
    return this.store;      
}
};
Ext.onReady(ContabilidadEditar.main.init, ContabilidadEditar.main);
</script>
<div id="formularioAgregar"></div>