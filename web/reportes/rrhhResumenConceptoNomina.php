<?php
include("ConexionComun.php");
include('fpdf.php');

class PDF extends FPDF {
    public $title;
    public $conexion;
    function Header() {

        $this->empresa = $this->getDatosEmpresa(1);

        // if(!empty($this->empresa['tx_imagen_izq'])){
        //     $this->Image("imagenes/".$this->empresa['tx_imagen_izq'], $this->empresa['izquierda_x'], $this->empresa['izquierda_y'], $this->empresa['izquierda_w']);
        // }

        $this->SetFont('Arial','B',10);

        $this->SetTextColor(0,0,0);
        $this->SetY(10);
        $this->Cell(0,0,utf8_decode('REPUBLICA BOLIVARIANA DE VENEZUELA'),0,0,'C');
        $this->Ln(6);
        $this->Cell(0,0,utf8_decode('GOBERNACION DEL ESTADO ZULIA'),0,0,'C');
        /*$this->Ln(6);
        $this->Cell(0,0,utf8_decode($this->empresa['nb_empresa']),0,0,'C');*/
        $this->Ln(6);
        $this->Cell(0,0,utf8_decode('RESUMEN DE CONCEPTOS POR NOMINAS'),0,0,'C');
        $this->SetFont('Arial','',8);

        $this->Cell(0,0,utf8_decode('Maracaibo, '.date("d").' de '.mes(date("m")).' del '.date("Y")),0,0,'R');
        $this->Ln(2);
        if ($this->PageNo()>1) $this->Cell(0,10,utf8_decode('Página ').$this->PageNo(),0,0,'R');  
       
        $this->SetTextColor(0,0,0);
        $this->SetX(1);       

    }

    function Footer() {
        $this->SetFont('Arial','',9);     
        $this->SetY(-20);
        $this->Cell(0,0,utf8_decode('Gobierno Electronico .:: GOBEL ::.'),0,0,'C');        
    }

    function dwawCell($title,$data) {
        $width = 8;
        $this->SetFont('Arial','B',12);
        $y =  $this->getY() * 20;
        $x =  $this->getX();
        $this->SetFillColor(206,230,100);
        $this->MultiCell(175,8,$title,0,1,'L',0);
        $this->SetY($y);
        $this->SetFont('Arial','',12);
        $this->SetFillColor(206,230,172);
        $w=$this->GetStringWidth($title)+3;
        $this->SetX($x+$w);
        $this->SetFillColor(206,230,172);
        $this->MultiCell(175,8,$data,0,1,'J',0);

    }

    function ChapterBody() {

        $listaArreglo  = json_decode($_GET['codigo'],true);
        $i=0;
        $contador = count($listaArreglo);

        foreach($listaArreglo as $arregloForm){

            $i++;

            $this->datos_lista = $this->getListaNomina($arregloForm['co_nomina']);

            $this->Ln(5);
            $this->setX(10);
            $this->SetFont('Arial','',8);     
            $this->SetWidths(array(30, 50, 20, 20, 20, 20 ));  
            $this->SetAligns(array("L",));   
            $this->Row(array('PROCESO: ', $this->datos_lista["co_solicitud"]),0,0); 
            $this->Ln(1);
            $this->Row(array(utf8_decode('PERIODO:'), strtoupper(utf8_decode($this->datos_lista["nu_tp_nomina"].' - '.$this->datos_lista["tx_tp_nomina"])), utf8_decode('DESDE:'), $this->datos_lista["fe_inicio"], utf8_decode('HASTA:'), $this->datos_lista["fe_fin"]),0,0); 
            $this->Ln(1);
            $this->Row(array(utf8_decode('ORGANISMO:'), utf8_decode('GOBERNACION DEL ZULIA')),0,0); 
            $this->Ln(1);
            $this->Row(array(utf8_decode('GRUPO:'), $this->datos_lista["nu_tp_nomina"]),0,0); 

            $this->Ln(5);
            $this->SetFont('Arial','B',7);     
            $this->SetWidths(array(10,50,20,30,30,30,30 ));   
            $this->SetAligns(array("C","C","C","C","C","C","C","C"));   
            $this->Row(array(utf8_decode('Nº'),'CONCEPTOS','CANT','ASIGNACION', 'DEDUCION', 'APORTE', 'NETO'),1,0); 
            $this->SetAligns(array("R","L","R","R","R","R","R"));

            $this->concepto_lista = $this->getListaConcepto($arregloForm['co_nomina']);

            $mo_total_asignacion = 0;
            $mo_total_deduccion = 0;
            $mo_total_patronal = 0;
            $mo_total_general = 0;

            foreach($this->concepto_lista as $key => $campo_concepto){

                $this->concepto_cantidad = $this->getCantidadConcepto($arregloForm['co_nomina'], $campo_concepto['nu_concepto']);

                $this->setX(10);
                $this->SetFont('Arial','',7);     
                $this->SetWidths(array(10,50,20,30,30,30,30 )); 
                $this->Row(array( $campo_concepto['nu_concepto'], utf8_decode($campo_concepto['de_concepto']), 
                //number_format($campo_concepto['nu_cantidad'], 2, ',','.'), 
                $this->concepto_cantidad['nu_cantidad'],
                number_format($campo_concepto['mo_asignacion'], 2, ',','.'), 
                number_format($campo_concepto['mo_deduccion'], 2, ',','.'), 
                number_format($campo_concepto['mo_patronal'], 2, ',','.'),
                number_format($campo_concepto['mo_concepto'], 2, ',','.') ),1,0);

                $mo_total_asignacion = $campo_concepto['mo_asignacion'] + $mo_total_asignacion;
                $mo_total_deduccion = $campo_concepto['mo_deduccion'] + $mo_total_deduccion;
                $mo_total_patronal = $campo_concepto['mo_patronal'] + $mo_total_patronal;

            }

            $this->setX(10);
            $this->SetFont( 'Arial', 'B', 9);
            $this->SetWidths(array(80,30,30,30,30 )); 
            $this->SetAligns(array("R","R","R","R","R"));
            $this->Row(array(utf8_decode('TOTAL NOMINA'), 
            number_format($mo_total_asignacion, 2, ',','.'), 
            number_format($mo_total_deduccion, 2, ',','.'), 
            number_format($mo_total_patronal, 2, ',','.'), 
            number_format($mo_total_asignacion-$mo_total_deduccion, 2, ',','.') ),1,0);

            if($i<$contador){
                $this->AddPage();
            }
            
        }

    }

    function ChapterTitle($num,$label) {
        $this->SetFont('Arial','',10);
        $this->SetFillColor(200,220,255);
        $this->Cell(0,6,"$label",0,1,'L',1);
        $this->Ln(8);
    }

    function SetTitle($title) {
        $this->title   = $title;
    }

    function PrintChapter() {
        $this->AddPage();
        $this->ChapterBody();
    }

    function getListaNomina($co_nomina){  

        $conex = new ConexionComun();
        
        $sql = "SELECT co_nomina, co_solicitud, tbrh013.co_tp_nomina, co_nom_frecuencia_pago, 
            tbrh013.nu_nomina, de_nomina, de_observacion, 
            in_procesado, id_tbrh060_nomina_estatus, cod_grupo_nomina, tx_grupo_nomina,
            tbrh013.co_grupo_nomina, tx_tp_nomina, tbrh017.nu_nomina as nu_tp_nomina,
            to_char(fe_inicio, 'DD-MM-YYYY') as fe_inicio,
            to_char(fe_fin, 'DD-MM-YYYY') as fe_fin,
            to_char(fe_pago, 'DD-MM-YYYY') as fe_pago
        FROM tbrh013_nomina as tbrh013
        INNER JOIN tbrh017_tp_nomina AS tbrh017 on tbrh013.co_tp_nomina = tbrh017.co_tp_nomina
        INNER JOIN tbrh067_grupo_nomina AS tbrh067 on tbrh013.co_grupo_nomina = tbrh067.co_grupo_nomina
        WHERE tbrh013.co_nomina = ".$co_nomina.";";
        
        //echo var_dump($sql); exit();
        
        //return $conex->ObtenerFilasBySqlSelect($sql);
        $datosSol = $conex->ObtenerFilasBySqlSelect($sql);
        return  $datosSol[0];

    }

    function getListaConcepto($co_nomina){  

        $conex = new ConexionComun();
        
        $sql = "SELECT de_fun, nu_concepto, de_concepto, sum(mo_concepto) as mo_concepto,
        CASE WHEN de_fun = 'A' THEN 
            sum(mo_concepto)
        END AS mo_asignacion,
        CASE WHEN de_fun = 'D' THEN 
            sum(mo_concepto)
        END AS mo_deduccion,
        CASE WHEN de_fun = 'P' THEN 
            sum(mo_concepto)
        END AS mo_patronal
            FROM tbrh101_cierre_nomina_presupuesto
            WHERE id_tbrh013_nomina = ".$co_nomina."
            group by 1 ,2, 3
            order by 1, CAST(nu_concepto AS BIGINT) asc;";
        
        //echo var_dump($sql); exit();
        
        return $conex->ObtenerFilasBySqlSelect($sql);

    }

    function getCantidadConcepto($co_nomina, $nu_concepto){  

        $conex = new ConexionComun();
        
        $sql = "SELECT COUNT(DISTINCT id_tbrh002_ficha) as nu_cantidad
            FROM tbrh061_nomina_movimiento as tbrh061
            INNER JOIN tbrh014_concepto AS tbrh014 on tbrh014.co_concepto = tbrh061.id_tbrh014_concepto
            WHERE id_tbrh013_nomina = ".$co_nomina." 
            AND nu_concepto = '".$nu_concepto."';";
        
        //echo var_dump($sql); exit();
        
        $datosSol = $conex->ObtenerFilasBySqlSelect($sql);
        return  $datosSol[0];

    }

    function getDatosEmpresa( $codigo){

        $sql = "SELECT co_empresa, nb_empresa, co_estado, co_municipio, tx_rif, tx_nit, 
        tx_direccion, tx_imagen_der, tx_imagen_izq, tx_imagen_cen, nu_telefono, 
        tx_sigla,
        op_imagen->'izquierda'->0 as izquierda_x,
        op_imagen->'izquierda'->1 as izquierda_y,
        op_imagen->'izquierda'->2 as izquierda_w,
        op_imagen->'centro'->0 as centro_x,
        op_imagen->'centro'->1 as centro_y,
        op_imagen->'centro'->2 as centro_w,
        op_imagen->'derecha'->0 as derecha_x,
        op_imagen->'derecha'->1 as derecha_y,
        op_imagen->'derecha'->2 as derecha_w
        FROM public.tb015_empresa
        WHERE co_empresa = ".$codigo.";";

        $conex = new ConexionComun();
        $datosSol = $conex->ObtenerFilasBySqlSelect($sql);
        return  $datosSol[0];
  
    }

}

$pdf=new PDF('P','mm','letter');
$pdf->PrintChapter();
$pdf->SetDisplayMode('default');
$pdf->Output(); 

?>